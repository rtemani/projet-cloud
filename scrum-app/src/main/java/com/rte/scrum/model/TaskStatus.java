package com.rte.scrum.model;

import io.swagger.annotations.ApiModel;

@ApiModel
public enum TaskStatus {
    TODO, INPROGRESS, DONE
}
